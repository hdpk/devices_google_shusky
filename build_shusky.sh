#!/bin/bash
# SPDX-License-Identifier: GPL-2.0

# Build script for Husky/Shiba with dynamic versioning
hdkv=v12.0

# Get the current branch name of the 'private/google-modules/soc/gs' repository
branch_name=$(git --git-dir=private/google-modules/soc/gs/.git symbolic-ref --short HEAD 2>/dev/null)

# mkcompile_h path
mkcomph=aosp/scripts/mkcompile_h

# Update CONFIG_LOCALVERSION in gki_defconfig
sed -i "s/LCH=.*/LCH=SkyDragon-$hdkv/" $mkcomph

# Determine local version based on branch name
if [[ $branch_name == *"lp"* ]]; then
    localversion="HDK.LP.$hdkv"
elif [[ $branch_name == *"6.1"* ]]; then
    localversion="HDK-6.1.$hdkv"
else
    localversion="HDK.$hdkv"
fi

# Clear the build.log file at the start of each run
if [ -f "build.log" ]; then
  echo "Resetting build.log for new start"
  rm build.log
  # Then remove any previously compiled files if they exist
  if [ -d "out/bazel" ]; then
    echo "Removing previously generated files for new build.." | tee -a build.log
    rm -rf out/*
    if [ $? -ne 0 ]; then
      echo "Error: Failed to remove previously generated files." | tee -a build.log
      exit 1
    else
      echo "Cleaned previously generated files!" | tee -a build.log
    fi  
  # Clean the Bazel build environment
    echo "Cleaning the Bazel build environment..." | tee -a build.log
    tools/bazel clean --expunge
    if [ $? -ne 0 ]; then
      echo "Error: Failed to clean the Bazel build environment." | tee -a build.log
      exit 1
    else
      echo "Starting Fresh Shusky build..." | tee -a build.log
    fi
  fi
else
  echo "Starting fresh Shusky build.." | tee -a build.log
fi

function exit_if_error {
  if [ $1 -ne 0 ]; then
    echo "ERROR: $2: retval=$1" >&2
    exit $1
  fi
}

# Changelog Generation function
function generate_changelog {
    local ak3_dir="anykernel3"
    local changelog_dir="${ak3_dir}/Changelog"
    
    # Check if the Changelog directory already exists
    if [ -d "$changelog_dir" ]; then
        echo "Changelog directory already exists." | tee -a build.log
    else
        # Create the Changelog directory if it doesn't exist
        mkdir -p "$changelog_dir" || {
            echo "Error: Failed to create the Changelog directory." | tee -a build.log
            return 1
        }
    fi

    # Path to the default.xml file
    local manifest_file="android/default.xml"

    # Extract repository paths from the manifest file
    local repos=($(grep 'remote="gitlab"' "$manifest_file" | sed -e 's/.*path="\([^"]*\)".*/\1/'))

    for repo in "${repos[@]}"; do
        local filename=$(basename "$repo").txt
        local repo_dir="${repo}/.git"  # Assuming the repositories are cloned and have a .git directory

        # Check if the repository directory exists and is a valid git repository
        if [ -d "$repo_dir" ]; then
            echo "Generating changelog for $repo..." | tee -a build.log
            git --git-dir="$repo_dir" log --pretty=format:"%h %s [%an]" --abbrev-commit --since="180 days ago" > "${changelog_dir}/${filename}"
            
            if [ $? -eq 0 ]; then
                echo "Changelog created successfully for $repo at ${changelog_dir}/${filename}" | tee -a build.log
            else
                echo "Error: Failed to create changelog for $repo" | tee -a build.log
                return 1
            fi
        else
            echo "Warning: '$repo' is not a valid git repository. Skipping..." | tee -a build.log
        fi
    done
}

# Kernel packaging function
function package_kernel {
    local dist_dir="out/shusky/dist"
    local ak3_dir="anykernel3"
    local upload_dir="upload"
    local localversion="$1"
    
    # Check if the upload directory exists
    if [ ! -d "${upload_dir}" ]; then
        echo "Creating ${upload_dir} directory..." | tee -a build.log
        # It doesn't, so we make the upload directory
        mkdir -p "${upload_dir}" | tee -a build.log
    else
        # It does, so we announce and move on
        echo "${upload_dir} directory already exists." | tee -a build.log
    fi
    
    # Call Changelog Generation function
    generate_changelog

    # Check if changelog was created successfully or not
    if [ $? -ne 0 ]; then
        echo "Error: Changelog generation failed." | tee -a build.log
        exit 1
    fi

    # Copy the required files
    echo "Copying kernel files to AnyKernel3 directory..." | tee -a build.log
    cp "${dist_dir}/boot.img" "${ak3_dir}/boot.img" | tee -a build.log
    cp "${dist_dir}/dtbo.img" "${ak3_dir}/dtbo.img" | tee -a build.log
    cp "${dist_dir}/system_dlkm.img" "${ak3_dir}/system_dlkm.img" | tee -a build.log
    cp "${dist_dir}/vendor_kernel_boot.img" "${ak3_dir}/vendor_kernel_boot.img" | tee -a build.log
    cp "${dist_dir}/vendor_dlkm.img" "${ak3_dir}/vendor_dlkm.img" | tee -a build.log
    # Check for errors
    if [ $? -ne 0 ]; then
        echo "Error: Failed to copy kernel files." | tee -a build.log
        return 1
    else
        echo "Kernel files successfully copied!" | tee -a build.log
    fi

    # Generate the zip file
    local zip_name="${localversion}.SHUSKY15.$(date +"%y%m%d.%H").zip"
    echo "Creating zip file: ${zip_name}..." | tee -a build.log
    # Navigate to anykernel3/ directory and zip files to upload/
    (cd "${ak3_dir}" && zip -r "../${upload_dir}/${zip_name}" *) | tee -a build.log

    # Check if zip creation was successful
    if [ $? -eq 0 ]; then
        echo "Kernel zip created successfully at ${upload_dir}/${zip_name}" | tee -a build.log
        echo "Removing old kernel files.." | tee -a build.log
        rm ${ak3_dir}/boot.img 2>&1 | tee -a build.log
        rm ${ak3_dir}/dtbo.img 2>&1 | tee -a build.log
        rm ${ak3_dir}/system_dlkm.img 2>&1 | tee -a build.log
        rm ${ak3_dir}/vendor_dlkm.img 2>&1 | tee -a build.log
        rm ${ak3_dir}/vendor_kernel_boot.img 2>&1 | tee -a build.log
        rm ${ak3_dir}/Changelog/*.txt 2>&1 | tee -a build.log
        echo "Cleaned anykernel directory!" | tee -a build.log
    else
        echo "Error: Failed to create kernel zip." | tee -a build.log
        return 1
    fi
}

parameters=
BUILD_AOSP_KERNEL=1
if [ "${BUILD_AOSP_KERNEL}" = "1" ]; then
  echo "WARNING: BUILD_AOSP_KERNEL is deprecated." \
    "Use --kernel_package=@//aosp instead." >&2
  parameters="--kernel_package=@//aosp"
fi

if [ "${BUILD_STAGING_KERNEL}" = "1" ]; then
  echo "WARNING: BUILD_STAGING_KERNEL is deprecated." \
    "Use --kernel_package=@//aosp-staging instead." >&2
  parameters="--kernel_package=@//aosp-staging"
fi

# Find `--page_size=16k` in the list of arguments, and append to bazel
# parameters to build a 16k page size kernel.
for arg in "$@"; do
  if [[ "$arg" == "--page_size=16k" ]]; then
    parameters+=" --page_size=16k"
    parameters+=" --config=no_download_gki"
    parameters+=" --config=no_download_gki_fips140"
  else
    remaining_args+=("$arg")
  fi
done

set -- "${remaining_args[@]}" # Update $@ with the remaining arguments

exec tools/bazel run \
    ${parameters} \
    --config=release \
    --config=shusky \
    //private/devices/google/shusky:zuma_shusky_dist "$@" 2>&1 | tee -a build.log

# Check for successful build and package kernel
if [ -f "out/shusky/dist/boot.img" ]; then
    # Run our function to make the flashable kernel zip
    echo "Generating Flashable Kernel Zip.." | tee -a build.log
    package_kernel "${localversion}"
    sed -i "s/LCH=.*/LCH=SkyDragon-/" $mkcomph
else
    # If no boot.img exists in out then cancel
    echo "Error: boot.img not found in ${dist_dir}. Bazel build may have failed or was cancelled." | tee -a build.log
    sed -i "s/LCH=.*/LCH=SkyDragon-/" $mkcomph
    exit 1
fi
